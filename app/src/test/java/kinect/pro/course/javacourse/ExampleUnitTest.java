package kinect.pro.course.javacourse;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
//        assertEquals(4, 2 + 2);
        assertTrue(Utils.isNumber("111"));
        assertTrue(Utils.isNumber("2394.123123"));
        assertTrue(Utils.isNumber("230402304023,402304"));
        assertFalse(Utils.isNumber("qkwekqwekk"));
        assertFalse(Utils.isNumber("23482348qeqwe"));
        assertFalse(Utils.isNumber("       "));
        assertFalse(Utils.isNumber("-+-+--+-+-+-+-+-+-"));
        assertFalse(Utils.isNumber("-"));
        assertFalse(Utils.isNumber("jqwej-=--+9+-09+47++qeqbfgh<>?|@@|#@#$&#$&(~"));
        assertTrue(Utils.isNumber("           `,`           "));
        assertFalse(Utils.isEqual("lsd", "skdk"));
        assertTrue(Utils.isEqual("qwe", "qwe"));
        assertFalse(Utils.isEqual("$$$#%", "$$$%#"));
        assertTrue(Utils.isString("qweqw"));
        assertFalse(Utils.isString("12312"));
        assertTrue(Utils.isString("   "));
        assertTrue(Utils.isString("++++"));
    }


}