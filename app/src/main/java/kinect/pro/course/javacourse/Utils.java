package kinect.pro.course.javacourse;


class Utils {


    static boolean isNumber(String enteredText) {
        if (!MainActivity.isString(enteredText)) {
            return true;
        } else {
            return (enteredText.contains(".")) | (enteredText.contains(","));
        }
    }

    static boolean isEqual(String mFirstInputText, String mSecondInputText) {
        return mFirstInputText.equals(mSecondInputText);
    }

    static boolean isString(String enteredText) {
        if (enteredText == null || enteredText.length() == 0) return true;
        int count = 0;
        if (enteredText.charAt(0) == '-') {
            if (enteredText.length() == 1) {
                return true;
            }
            count = 1;
        }

        char c;
        for (; count < enteredText.length(); count++) {
            c = enteredText.charAt(count);
            if (!(c >= '0' && c <= '9')) {
                return true;
            }
        }
        return false;
    }
}
