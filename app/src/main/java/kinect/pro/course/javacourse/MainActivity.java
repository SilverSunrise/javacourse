package kinect.pro.course.javacourse;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {

    EditText mFirstInputText;
    EditText mSecondInputText;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFirstInputText = findViewById(R.id.et_input_first_text);
        result = findViewById(R.id.tv_result);
        mSecondInputText = findViewById(R.id.et_input_second_text);
    }

    public void showType(View view) {

        String entered_text = mFirstInputText.getText().toString();
        if (!isString(entered_text)) {
            result.setText(R.string.show_type_text_integer);
        } else {
            if ((entered_text.contains(".")) | (entered_text.contains(","))) {
                result.setText(R.string.show_type_text_double);
            } else {
                result.setText(R.string.show_type_text_string);
            }
        }
    }

    public static boolean isString(String enteredText) {
        if (enteredText == null || enteredText.length() == 0) return true;
        int count = 0;
        if (enteredText.charAt(0) == '-') {
            if (enteredText.length() == 1) {
                return true;
            }
            count = 1;
        }


        for (; count < enteredText.length(); count++) {
            if (!(enteredText.charAt(count) >= '0' && enteredText.charAt(count) <= '9')) {
                return true;
            }
        }
        return false;
    }

    public void compareInputs(View view) {
        if (mFirstInputText.getText().toString().equals(mSecondInputText.getText().toString())) {
            result.setText(R.string.compare_inputs_text_true_equal);
        } else {
            result.setText(R.string.compare_inputs_text_false_equals);
        }
    }
}
